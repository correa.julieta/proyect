import React, { Component } from 'react';
import PropTypes from 'prop-types';
import "./App.css";
import Header from './header/menu';
import Footer from './footer/footer';
import Contenido from './contenido';
class App extends Component {
   constructor(props) {
    super(props);
    
   this.state = {
     link: [{nombre:"Inicio", href:"/home"},{nombre:"Categorias", href:""},
      {nombre:"Perfil", href:"/perfil"},{nombre:"About us", href:""}]
   };
 };
static propTypes={
  children: PropTypes.object.isRequired
};

  render() {
    const { children } = this.props;
    return ( 
   <div className="app">
   <div className="container-fluid">
     <Header links={this.state.link} color="azul" texto="Enki"></Header>
     <Contenido body={children}/>
     <Footer color="rosa"></Footer>
   </div>
   </div>
    );
  }
}

export default App;