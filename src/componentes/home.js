import React, { Component } from 'react';
import Card from '../cards/subcomponente';
import "../cards/subcomponente.css";
class Home extends Component {
	 constructor(props) {
    super(props);
    
   this.state = {
     item: []
   };
  }
 componentDidMount () {
  fetch('http://localhost/Enki3000/api/producto.php')
  .then(response => response.json())
  .then(data => {
    this.setState({
      item: data.response
    });
  })
  .catch(err => {
    console.log(err);
  })

}
	render(){
		return(

      <div className="homec">
       
          <Card items={this.state.item}></Card>

      </div>
     
			);
	}
}
export default Home;