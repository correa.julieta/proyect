import React, { Component } from 'react'
import "./footer.css";
import "../fonts/style.css"
class Footer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      texto: "Soy un Footer"
    };
  }

  render() {
    return ( 
    <footer className="container-fluid"><div className="footer">
      <div className="redes">
      <ul className="barra">
    <li className="title">Siguenos</li>
    <li><a href="#" className="icon-facebook"></a>Enkimmerce</li>
    <li><a href="#" className="icon-twitter"></a>@Eki_mmerce</li>
    <li><a href="#" className="icon-mail"></a>Enki.mmerce@gmail.com</li>
  </ul></div>
  <div className="contacto">
    <p className="title">Contactanos!</p>
    <form>
      <label >Mail: </label><input type="email" name="mail"/>
      <label >Mensaje: </label><input type="text" name="mensaje"/>
      <input type="submit" name="enviar" value="Enviar"/>
    </form>
  </div>
<div className="nosotros">
  <ul >
    <li className="title">About us</li>
    <li><a href="#">Inicios</a></li>
    <li><a href="#">Inspiracion</a></li>
    <li><a href="#">Metas</a></li>
  </ul>
  
</div> </div>
  <p className="pfo">&copy; Enki Commerce</p>  
   </footer>

    );
  }
}

export default Footer;