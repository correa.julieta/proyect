import React, { Component } from 'react'
import "./menu.css";
import Buscador from './buscador';
import { Link } from "react-router-dom";
class Menu extends Component {

      
  render() {
  return (
       <nav className={`navbar navbar-expand-lg navbar-dark ${ this.props.color }`}>
    <Link className="navbar-brand" to="/home" >{this.props.texto}</Link>
     <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav">    {this.props.links.map(
            (link,index) =>(
        <li className="nav-item" key={index}>
          <Link className="nav-link" to={link.href}>{link.nombre}</Link>
        </li>
         )
            )}
        
      </ul>
      </div>
    <Buscador/> 
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
      </button>
    </nav>
   
    );
  }
}

export default Menu;