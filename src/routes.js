import React from 'react';
import {Route, Switch} from 'react-router-dom';
import App from './App';
import Home from './componentes/home';
import Perfil from './componentes/perfil';
import Page404 from './componentes/page404';
const AppRoutes= ()=>
<App>
<Switch>
<Route path="/home" component={Home}/>
<Route path="/perfil" component={Perfil}/>
<Route component={Page404}/>
</Switch>
</App>;
export default AppRoutes;